import json
from napalm import get_network_driver
driver = get_network_driver('ios')
iosvl2 = driver ('10.10.20.51', 'cisco', 'cisco')
iosvl2.open()

ios_output = iosvl2.get_facts()
print(json.dumps (ios_output, indent=4))

ios_output = iosvl2.get_interfaces()
print(json.dumps (ios_output,sort_keys=True, indent=4))

ios_output = iosvl2.get_interfaces_counters()
print(json.dumps (ios_output, sort_keys=True, indent=4))

ios_output = iosvl2.get_interfaces_ip()
print(json.dumps (ios_output, indent=4))

ios_output = iosvl2.get_arp_table()
print(json.dumps (ios_output, indent=4))

ios_output = iosvl2.get_arp_table()
print(json.dumps (ios_output, indent=4))

ios_output = iosvl2.ping('10.10.20.13')
print(json.dumps (ios_output, indent=4))
