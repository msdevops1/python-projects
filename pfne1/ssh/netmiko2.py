from netmiko import ConnectHandler

iosv_l2_dc01 = {
    'device_type': 'cisco_ios',
    'ip': '10.10.20.51',
    'username': 'cisco',
    'password': 'cisco'
}

iosv_l2_dc02 = {
    'device_type': 'cisco_ios',
    'ip': '10.10.20.52',
    'username': 'cisco',
    'password': 'cisco'
}

iosv_l2_sw01 = {
    'device_type': 'cisco_ios',
    'ip': '10.10.20.13',
    'username': 'cisco',
    'password': 'cisco'
}

all_devices = [iosv_l2_dc01, iosv_l2_dc02, iosv_l2_sw01]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    for n in range (2,21):
        print ("Creating VLAN " + str(n))
        config_commands = ['vlan ' + str(n), 'name Python_VLAN ' + str(n)]
        output = net_connect.send_config_set(config_commands)
        print (output)
