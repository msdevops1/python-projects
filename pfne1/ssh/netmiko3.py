from netmiko import ConnectHandler

iosv_l2_dc02 = {
    'device_type': 'cisco_ios',
    'ip': '10.10.20.52',
    'username': 'cisco',
    'password': 'cisco'
}

iosv_l2_dc01 = {
    'device_type': 'cisco_ios',
    'ip': '10.10.20.51',
    'username': 'cisco',
    'password': 'cisco'
}

with open('iosv_l2_cisco_design') as f:
    lines = f.read() .splitlines()
print (lines)

all_devices = [iosv_l2_dc02, iosv_l2_dc01]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    output = net_connect.send_config_set(lines)
    print (output)
