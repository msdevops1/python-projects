import paramiko
import time
import getpass


username = input('Username: ')
password = getpass.getpass('password: ')

f = open("ip_list.txt")

for line in f.readlines():
    ip_address = line.strip()
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=ip_address,username=username,password=password,look_for_keys=False)
    print ("Successfully connect to ", ip_address)
    command = ssh_client.invoke_shell()
    command.send("show ver\n")
    time.sleep(2)
    output = command.recv(65535)
    print (output.decode('ascii'))

f.close()
ssh_client.close