import time
import datetime
import getpass
import csv

from netmiko import BaseConnection
from napalm import get_network_driver
from netmiko import NetMikoAuthenticationException, NetMikoTimeoutException
from napalm.base.exceptions import ConnectionException
from concurrent.futures import ThreadPoolExecutor

BANNER = '''
        ################################################
        #       Python Scrip for Cisco Devices         #
        #     Please be patient sit back and relax     # 
        ################################################
        # Version: 1.0    Author & Copyright: MBS 2020 #
        ################################################
     '''

print(BANNER)

while True:
    try:
        MAX_WORKERS = input("Maximum Workers: ")
        MAX_WORKERS = int(MAX_WORKERS)
        break

    except ValueError:
        print("Error: Value for Maximum Workers must be an Integer !\n")

print("\n Enter Centralized username & Password \n")
get_username = input("Username: ")
get_password = getpass.getpass(prompt="Password: ")

start_timer = time.perf_counter()

with open("host_file.txt") as f:
    data = f.read()

ip_list = []
for n in data.split():
    ip_list.append(n)

FILE_NAME_FACTS = "show_version.csv"

data_file = open(FILE_NAME_FACTS, "w")
fieldnames = ["HOSTNAME", "VENDOR", "IP", "MODEL", "OS_VERSION", "UPTIME"]
thewriter = csv.DictWriter(data_file, fieldnames=fieldnames)
thewriter.writeheader()

file_object_fail = open("failed_devices.txt", "w")

print("\nConnecting to network devices ....\n")


def err_msg(ip, msg):
    '''Function for creating message & saving device ip to 'failed_devices.txt'
        in case of any exception'''
    print("\n" + "=" * 55)
    print(f"Error: {msg} for {ip}!")
    print("=" * 55 + "\n")
    file_object_fail.write(f'{ip}\n')


def get_data(ip):
    '''Fetching HOSTNAME,IP,MODEL,OS_VERSION,VENDOR,UPTIME for each device
        and saving it to a CSV file'''
    try:
        # Check device OS and selecting network driver accordingly
        connect = BaseConnection(host=ip, username=get_username, password=get_password)
        connect.find_prompt()

        print(f"Connected Successfully  to {ip}")
        data = connect.send_command("show version")
        connect.disconnect()

        if "cisco nexus" in data.lower():
            type = "nxos_ssh"
        elif "cisco ios xr" in data.lower():
            type = "iosxr"
        else:
            type = "ios"

        connect_dev = get_network_driver(type)
        device = connect_dev(hostname=ip, username=get_username, password=get_password)
        device.open()
        session_log = 'my_session.txt'

        final_data = device.get_facts()
        print(f"Collecting output from {ip}")
        device.close()

        uptime = final_data["uptime"]
        uptime = datetime.timedelta(seconds=uptime)
        vendor = final_data["vendor"]
        model = final_data["model"]
        os_version = final_data["os_version"]
        hostname = final_data["hostname"]

        item_list = [hostname, vendor, ip, model, os_version, uptime]

        thewriter = csv.writer(data_file)
        thewriter.writerow(item_list)

    except NetMikoAuthenticationException:
        err_msg(ip, "Authentication Failure")

    except (NetMikoTimeoutException, ConnectionException):
        err_msg(ip, "Timed Out")

    except:
        print(f"Unknown Error for {ip}")


if __name__ == "__main__":
    ''' Executing our get_data function over list of hosts using
        multithreading'''
    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        execute = executor.map(get_data, ip_list)

    data_file.close()
    file_object_fail.close()

    print(f"\nOutput Saved to '{FILE_NAME_FACTS}'!")

    end_timer = time.perf_counter()
    timer = f'\nExecuted in = {round(end_timer - start_timer, 1)} second(s)'
    print(timer)