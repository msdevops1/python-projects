import time  # Importing Time module
import datetime  # Date&Time module
import \
    getpass  # The getpass function is used to prompt to users to enter the password during the script running (Portable password input)
import csv  # Enable the CSV file function in the script

from netmiko import BaseConnection  # Handles SSH connection
from napalm import \
    get_network_driver  # Starting by importing the NAPALM module into Python, in our script we are using NAPALM feature to get the required output
from netmiko import NetMikoAuthenticationException, \
    NetMikoTimeoutException  # adding error exception if incase any device doesnt authenticate or timeout to avoice script breaking.
from napalm.base.exceptions import ConnectionException  # part of above connection exception
from concurrent.futures import \
    ThreadPoolExecutor  # importing function into script to enable Multi-Treading feature to speed up the process.

# Banner
BANNER = '''
        #################################################

                        YOUR BANNER HERE        

        #################################################
     '''

print(BANNER)

# Asking user for Maximum number of threads to use during the script.
# Script  will ask  to enter the number of worker that you want to use.  its always depends on number of devices and available threads on local machine where your running the script.
while True:
    try:
        MAX_WORKERS = input("Maximum Workers: ")
        MAX_WORKERS = int(MAX_WORKERS)
        break

    except ValueError:
        print("Error: Value for Maximum Workers must be an Integer !\n")

# Getting credentials from user to enter username & Password
print("\n Enter Centralized username & Password \n")
get_username = input("Username: ")
get_password = getpass.getpass(prompt="Password: ")

# Starting timer recorded into the script once completed we will have the total time.
start_timer = time.perf_counter()

# Opening a host file to read IPs
with open("host_file.txt") as f:
    data = f.read()
# Creating a list of the host file.
ip_list = []
for n in data.split():
    ip_list.append(n)

# Creating a CSV file
FILE_NAME_FACTS = "show_version.csv"

# Creating the appropriate headers in CSV file to save data.
data_file = open(FILE_NAME_FACTS, "w")
fieldnames = ["HOSTNAME", "VENDOR", "IP", "MODEL", "OS_VERSION", "UPTIME"]
thewriter = csv.DictWriter(data_file, fieldnames=fieldnames)
thewriter.writeheader()

# Creating file for saving IPs, that failed
file_object_fail = open("failed_devices.txt", "w")

# Printing the output on the  screen that device connecting
print("\nConnecting to network devices ....\n")


# Function for creating message & saving device ip to 'failed_devices.txt' in case of any exception, This feature avoide the script to break.
def err_msg(ip, msg):
    '''Function for creating message & saving device ip to 'failed_devices.txt'
        in case of any exception'''
    print("\n" + "=" * 55)
    print(f"Error: {msg} for {ip}!")
    print("=" * 55 + "\n")
    file_object_fail.write(f'{ip}\n')


def get_data(ip):
    '''Fetching HOSTNAME,IP,MODEL,OS_VERSION,VENDOR,UPTIME for each device
        and saving it to a CSV file'''
    try:
        # Check device OS and selecting network driver accordingly
        connect = BaseConnection(host=ip, username=get_username, password=get_password)
        connect.find_prompt()

        print(f"Connected Successfully  to {ip}")
        data = connect.send_command("show version")
        connect.disconnect()

        if "cisco nexus" in data.lower():
            type = "nxos_ssh"
        elif "cisco ios xr" in data.lower():
            type = "iosxr"
        else:
            type = "ios"

        # Getting Data from network devices using NAPALM python module
        connect_dev = get_network_driver(type)
        device = connect_dev(hostname=ip, username=get_username, password=get_password)
        device.open()
        final_data = device.get_facts()
        print(f"Collecting output from {ip}")
        device.close()

        uptime = final_data["uptime"]
        uptime = datetime.timedelta(seconds=uptime)
        vendor = final_data["vendor"]
        model = final_data["model"]
        os_version = final_data["os_version"]
        hostname = final_data["hostname"]

        item_list = [hostname, vendor, ip, model, os_version, uptime]

        # Writing list of data items in CSV file with above mentioned parser uptime, vendor, model, version, hostname.
        thewriter = csv.writer(data_file)
        thewriter.writerow(item_list)

    # if incase any device not able to authenticate we will get the message on the screen that "Authentication Failure with Device IP address".
    except NetMikoAuthenticationException:
        err_msg(ip, "Authentication Failure")

    # Device that not reachable  we will get the message on the screen that "Time Out with device IP"
    except (NetMikoTimeoutException, ConnectionException):
        err_msg(ip, "Timed Out")
    # Other than above message will be printed as " Unknown Error with device IP for our information.
    except:
        print(f"Unknown Error for {ip}")


# Executing the get data function over ip_list of using multi-hreading.
if __name__ == "__main__":
    ''' Executing our get_data function over list of hosts using
        multithreading'''
    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        execute = executor.map(get_data, ip_list)

    data_file.close()
    file_object_fail.close()

    print(f"\nOutput Saved to '{FILE_NAME_FACTS}'!")

    # Stoping Timer once completed the script
    end_timer = time.perf_counter()
    # Printing Total Execution time of the Script
    timer = f'\nExecuted in = {round(end_timer - start_timer, 1)} second(s)'
    print(timer)