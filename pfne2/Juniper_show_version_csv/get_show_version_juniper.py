import time
import datetime
import getpass
import csv

from netmiko import Netmiko
from netmiko import NetMikoAuthenticationException, NetMikoTimeoutException
from concurrent.futures import ThreadPoolExecutor


# Banner
BANNER = '''
        ################################################
        #       Python Scrip for Juniper Devices       #
        #     Please be patient sit back and relax     # 
        ################################################
        # Version: 1.0    Author & Copyright: MBS 2020 #
        ################################################
     '''

print(BANNER)

while True:
    try:
        MAX_WORKERS = input("Maximum Workers: ")
        MAX_WORKERS = int(MAX_WORKERS)
        break

    except ValueError:
        print("Error: Value for Maximum Workers must be an Integer !\n")

print("\n Enter Centralized username & Password \n")
get_username = input("Username: ")
get_password = getpass.getpass(prompt="Password: ")

start_timer = time.perf_counter()

with open("host_file.txt") as f:
    data = f.read()

ip_list = []
for n in data.split():
    ip_list.append(n)

FILE_NAME_FACTS = "show_version.csv"

data_file = open(FILE_NAME_FACTS, "w")
fieldnames = ["HOSTNAME", "VENDOR", "IP", "MODEL", "OS_VERSION", "UPTIME"]
thewriter = csv.DictWriter(data_file, fieldnames=fieldnames)
thewriter.writeheader()

file_object_fail = open("failed_devices.txt", "w")

print("\nConnecting to network devices....\n")


def err_msg(ip, msg):
    '''Function for creating message & saving device ip to 'failed_devices.txt'
        in case of any exception'''
    print("\n" + "=" * 55)
    print(f"Error: {msg} for {ip}!")
    print("=" * 55 + "\n")
    file_object_fail.write(f'{ip}\n')


def get_data(ip):
    '''Fetching HOSTNAME,IP,MODEL,OS_VERSION,VENDOR,UPTIME for each device
        and saving it to a CSV file'''
    try:
        connect = Netmiko(host=ip, username=get_username, password=get_password,
                          device_type="juniper")

        print(f"Connected Successfully  to  {ip} ")

        version_junos = connect.send_command("show version")
        hostname_junos = connect.send_command("show configuration | grep host-name ")
        uptime_junos = connect.send_command("show system uptime")

        print(f"Collecting output from {ip}")

        connect.disconnect()

        for n in version_junos.splitlines():
            if "Model" in n:
                model = n.split(":")[1].strip()
            elif "Junos:" in n:
                vendor = "Juniper"
                os_version = n.split(":")[1].strip()
            else:
                continue

        for u in uptime_junos.splitlines():
            if "System booted" in u:
                upt = u.split("(")[1].rstrip(")")[:-3].strip()
            else:
                continue

        if len(hostname_junos) == 0:
            host = "None"
        else:
            host = hostname_junos.split()[1][:-1]

        data = [host, vendor, ip, model, os_version, upt]

        thewriter = csv.writer(data_file)
        thewriter.writerow(data)

    except NetMikoAuthenticationException:
        err_msg(ip, "Authentication Failure")

    except (NetMikoTimeoutException):
        err_msg(ip, "Timed Out")

    except:
        print(f"Unknown Error for {ip}")


if __name__ == "__main__":
    ''' Executing our get_data function over list of hosts using
        multithreading'''
    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        execute = executor.map(get_data, ip_list)

    data_file.close()
    file_object_fail.close()

    print(f"\nOutput Saved to '{FILE_NAME_FACTS}'!")

    end_timer = time.perf_counter()
    # Printing Total Execution time of the Script
    timer = f'\nExecuted in = {round(end_timer - start_timer, 1)} second(s)'
    print(timer)