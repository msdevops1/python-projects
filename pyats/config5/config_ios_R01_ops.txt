{
  "Building configuration...": {},
  "Current configuration : 1456 bytes": {},
  "boot-end-marker": {},
  "boot-start-marker": {},
  "bsd-client server url https://cloudsso.cisco.com/as/token.oauth2": {},
  "clock timezone MST -7 0": {},
  "control-plane": {},
  "cts logging verbose": {},
  "enable password cisco": {},
  "end": {},
  "hostname R01": {},
  "interface Ethernet0/0": {
    "no ip address": {},
    "shutdown": {}
  },
  "interface Ethernet0/1": {
    "no ip address": {},
    "shutdown": {}
  },
  "interface Ethernet0/2": {
    "no ip address": {},
    "shutdown": {}
  },
  "interface Ethernet0/3": {
    "no ip address": {},
    "shutdown": {}
  },
  "interface Ethernet1/0": {
    "ip address 10.10.20.11 255.255.255.0": {}
  },
  "interface Ethernet1/1": {
    "no ip address": {},
    "shutdown": {}
  },
  "interface Ethernet1/2": {
    "no ip address": {},
    "shutdown": {}
  },
  "interface Ethernet1/3": {
    "no ip address": {},
    "shutdown": {}
  },
  "ip cef": {},
  "ip domain name cisco.com": {},
  "ip forward-protocol nd": {},
  "ip ssh version 2": {},
  "line aux 0": {},
  "line con 0": {
    "exec-timeout 0 0": {},
    "logging synchronous": {}
  },
  "line vty 0 4": {
    "login local": {},
    "transport input all": {},
    "transport output telnet": {}
  },
  "mmi polling-interval 60": {},
  "mmi snmp-timeout 180": {},
  "multilink bundle-name authenticated": {},
  "no aaa new-model": {},
  "no ip http secure-server": {},
  "no ip http server": {},
  "no ipv6 cef": {},
  "no logging console": {},
  "no mmi auto-configure": {},
  "no mmi pvc": {},
  "no service password-encryption": {},
  "redundancy": {},
  "router ospf 100": {
    "network 0.0.0.0 255.255.255.255 area 0": {}
  },
  "service timestamps debug datetime msec": {},
  "service timestamps log datetime msec": {},
  "username cisco privilege 15 password 0 cisco": {},
  "version 15.5": {}
}