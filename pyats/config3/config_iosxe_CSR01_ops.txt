{
  "Building configuration...": {},
  "Current configuration : 1480 bytes": {},
  "boot-end-marker": {},
  "boot-start-marker": {},
  "control-plane": {},
  "diagnostic bootup level minimal": {},
  "end": {},
  "hostname CSR01": {},
  "interface GigabitEthernet1": {
    "negotiation auto": {},
    "no ip address": {},
    "no mop enabled": {},
    "no mop sysid": {},
    "shutdown": {}
  },
  "interface GigabitEthernet2": {
    "negotiation auto": {},
    "no ip address": {},
    "no mop enabled": {},
    "no mop sysid": {},
    "shutdown": {}
  },
  "interface GigabitEthernet3": {
    "negotiation auto": {},
    "no ip address": {},
    "no mop enabled": {},
    "no mop sysid": {},
    "shutdown": {}
  },
  "interface GigabitEthernet4": {
    "ip address 10.10.20.31 255.255.255.0": {},
    "negotiation auto": {},
    "no mop enabled": {},
    "no mop sysid": {}
  },
  "ip forward-protocol nd": {},
  "ip http authentication local": {},
  "ip http secure-server": {},
  "ip http server": {},
  "license udi pid CSR1000V sn 9CAAQQDFDUX": {},
  "line con 0": {
    "exec-timeout 0 0": {},
    "logging synchronous": {},
    "stopbits 1": {}
  },
  "line vty 0 4": {
    "login local": {},
    "transport input all": {},
    "transport output telnet": {}
  },
  "multilink bundle-name authenticated": {},
  "no aaa new-model": {},
  "no logging console": {},
  "no platform punt-keepalive disable-kernel-core": {},
  "platform console serial": {},
  "redundancy": {},
  "router ospf 100": {
    "network 0.0.0.0 255.255.255.255 area 0": {}
  },
  "service timestamps debug datetime msec": {},
  "service timestamps log datetime msec": {},
  "spanning-tree extend system-id": {},
  "subscriber templating": {},
  "username cisco privilege 15 password 0 cisco": {},
  "version 16.3": {},
  "virtual-service csr_mgmt": {}
}