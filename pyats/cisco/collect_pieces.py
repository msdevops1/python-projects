from netmiko import BaseConnection , NetmikoTimeoutException , NetMikoAuthenticationException
from napalm import get_network_driver
from napalm.base.exceptions import ConnectionException
import csv
import getpass
from concurrent.futures import ThreadPoolExecutor




get_username = input("Username: ")
get_password = getpass.getpass(prompt="Password: ")


with open("host_file.txt") as f:
    data = f.read().splitlines()


ip_list = []
for n in data:
    ip_list.append(n)


data_file = open("my_inventory.csv","w")
fieldnames = ["hostname","ip","username","password","os","protocol"]
thewriter = csv.DictWriter(data_file,fieldnames=fieldnames)
thewriter.writeheader()

file_object_fail = open("failed_devices.txt","w")

def err_msg(ip ,msg):
    '''Function for creating message & saving device ip to 'failed_devices.txt'
        in case of any exception'''
    print("\n" + "="*55)
    print(f"Error: {msg} for {ip}!")
    print("="*55 + "\n")
    file_object_fail.write(f'{ip}\n')


def main(ip):
    try:
        device = BaseConnection(host=ip  , username=get_username , password=get_password)
        check_version = device.send_command("show version")
        device.disconnect()

        if "cisco nexus" in check_version.lower():
            dev_type = "nxos_ssh"
        elif "cisco ios xr" in check_version.lower():
            dev_type = "iosxr"
        elif "cisco ios xe" in check_version.lower():
            dev_type = "iosxe"
        else:
            dev_type = "ios"

        connect_dev= get_network_driver(dev_type)
        device = connect_dev(hostname=ip ,username=get_username ,password=get_password)
        device.open()
        final_data=device.get_facts()
        print(f"Fetching data from {ip}")
        device.close()

        if dev_type == "nxos_ssh":
            dev_type = "nxos"

        hostname = final_data["hostname"]
        os = dev_type
        protocol = "ssh"

        item_list = [hostname, ip , get_username , get_password, os, protocol]

        thewriter = csv.writer(data_file)
        thewriter.writerow(item_list)


    except NetMikoAuthenticationException:
        err_msg(ip,"Authentication Failure")

    except NetmikoTimeoutException:
        err_msg(ip,"Timed Out")

    except:
        print(f"Unknown Error for {ip}")


if __name__ == "__main__":
    ''' Executing our get_data function over list of hosts using
        multithreading'''
    with ThreadPoolExecutor() as executor:
        execute = executor.map(main,ip_list)

    data_file.close()
    file_object_fail.close()

