DB_PASS = config["DB_PASS"]

db_string = "postgresql://db_name:" + DB_PASS + "@localhost:5432/db_name"

db = create_engine(db_string)
base = declarative_base()


# Create DB Tables
class TestCoreBgp(base):
    __tablename__ = 'test_core_bgp'

    pk_id = Column(Integer, primary_key=True)
    device = Column(String)
    passed_test = Column(Boolean)
    reason = Column(String)
    test = Column(String)
    dt = Column(DateTime(timezone=True), default=func.now())
    latest = Column(Boolean)


Session = sessionmaker(db)
session = Session()

base.metadata.create_all(db)

session.query(TestCoreBgp).filter(TestCoreBgp.latest == True). \
    update({TestCoreBgp.latest: TestCoreBgp.latest == False}, synchronize_session=False)


class CommonSetup(aetest.CommonSetup):

    @aetest.subsection
    def connect(self, testbed):
        '''
        Establishes a connection to all testbed devices.
        '''

        testbed = Genie.init('core_lab_testbed.yaml')
        devices = testbed.devices.values()

        device_list = []
        failed_device_list = []

        for device in devices:
            device_list.append(device)

        # make sure testbed is provided
        assert testbed, 'Testbed was not provided!'

        # connect to all testbed devices
        try:
            testbed.connect(init_exec_commands=[], init_config_commands=[], connection_timeout=5)
        except:
            for device in devices:
                if not device.connected:

                    # Results log to DB
                    DEVICE = device.name
                    PASSED_TEST = False
                    REASON = "unable to connect to device: '{}'".format(device.name)
                    TEST = "Initial Connection"
                    LATEST = True
                    row = TestCoreBgp(device=DEVICE, passed_test=PASSED_TEST, reason=REASON, test=TEST, latest=LATEST)
                    session.merge(row)
                    session.commit()
                    device_list.remove(device)
                    failed_device_list.append(device)

                    logger.warning("unable to connect to device: '{}'".format(device.name))

                    continue
                else:

                    # Results log to DB
                    DEVICE = device.name
                    PASSED_TEST = True
                    REASON = "Successfully connected to device: '{}'".format(device.name)
                    TEST = "Initial Connection"
                    LATEST = True
                    row = TestCoreBgp(device=DEVICE, passed_test=PASSED_TEST, reason=REASON, test=TEST, latest=LATEST)
                    session.merge(row)
                    session.commit()

                    logger.info("Successfully connected to device: '{}'".format(device.name))

                    continue

        self.parent.parameters.update(devices=device_list)

        if failed_device_list:
            self.passx("Unable to connect to all devices in testbed")
        else:
            self.passed("Successfully connected to all devices.")


1

