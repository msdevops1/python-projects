+++ CSR01: executing command 'show running-config' +++
show running-config
Building configuration...

Current configuration : 1630 bytes
!
! Last configuration change at 22:06:21 UTC Sun May 17 2020 by cisco
!
version 16.3
service timestamps debug datetime msec
service timestamps log datetime msec
no platform punt-keepalive disable-kernel-core
platform console serial
!
hostname CSR01
!
boot-start-marker
boot-end-marker
!
!
no logging console
!
no aaa new-model
!
!
!
!
!
!
!
!
!



!
!
!
!
!
!
!
!
!
!
subscriber templating
!
!
!
multilink bundle-name authenticated
!
!
!
!
!
!


!
!
!
!
!
!
!
license udi pid CSR1000V sn 9CAAQQDFDUX
diagnostic bootup level minimal
!
spanning-tree extend system-id
!
!
username cisco privilege 15 password 0 cisco
!
redundancy
!
!
!
!
!
!
! 
!
!
!
!
!
!
!
!
!
!
!
!
! 
! 
! 
! 
! 
! 
!
!
interface Loopback100
 ip address 100.100.100.100 255.255.255.255
!
interface GigabitEthernet1
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet2
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet3
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet4
 ip address 10.10.20.31 255.255.255.0
 negotiation auto
 no mop enabled
 no mop sysid
!
router ospf 100
 network 0.0.0.0 255.255.255.255 area 0
!
router bgp 1
 bgp log-neighbor-changes
 neighbor 1.1.1.1 remote-as 100
!
!
virtual-service csr_mgmt
!
ip forward-protocol nd
ip http server
ip http authentication local
ip http secure-server
!
!
!
!
!
!
!
control-plane
!
 !
 !
 !
 !
!
!
!
!
!
line con 0
 exec-timeout 0 0
 logging synchronous
 stopbits 1
line vty 0 4
 login local
 transport input all
 transport output telnet
!
!
!
!
!
!
end

CSR01#
