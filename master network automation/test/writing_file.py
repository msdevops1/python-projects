# with open('myfile1.txt', 'r+') as f:
#     f.write('\nABC\n')
#     f.write('This is a test file')
#
# with open('configuration.txt', 'r+') as f:
#     f.seek(5)
#     f.write('100')
#     f.write('Line added with r+\n')
#
#     f.seek(50)
#     print(f.read(15))
#
#     file.write('March 24:9000\n')  ## appending to the end of the file
#
# with open('configuration.txt', 'r') as my_file:
#     content = my_file.readlines()   # content is a list
#     print(content)
#
# file object is an iterable object
# with open('configuration.txt', 'r') as my_file:
#     for line in my_file:    #iterating over the lines within the file
#       print(line, end='')
#
# f = open('configuration.txt')



